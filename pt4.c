#include <stdio.h>
#include <stdlib.h>

typedef struct { int *buf;    size_t len;   size_t cap;
} IntVec;

IntVec iv_new(size_t cap){
        int *buf = (int *)malloc(cap* sizeof(int));
    IntVec vec =
            {buf,
                  0, cap};
    return
    vec;
}

int iv_at(IntVec *v, size_t i)
{ return v->buf[i];
}

int iv_last(IntVec *v)
{ return iv_at(v, v->len - 1); }

int iv_append(IntVec *v, int i) {
    if (v->len == v->cap) {
        v->cap *= 2;
        v->buf = (int *)realloc(v->buf, v->cap * sizeof(int));
    }
    v->buf[v->len] = i;
    v->len++;

return 0;    }

int iv_free(IntVec *v) {
    free(
        v->buf
        );
return 0;
}

int iv_print(FILE * dst, IntVec *v) {
    for (size_t i = 0; i < v->len; i++) {
    fprintf(dst, "%d ", iv_at(v, i));
    } printf("\n");
    return 0;
}

IntVec generate_partial_sums(IntVec *int_vector) {
    IntVec partial_sums = iv_new(int_vector->cap);
    iv_append(&partial_sums, iv_at(int_vector, 0));
    for (size_t i = 1; i < int_vector->len; i++) {
        iv_append(&partial_sums, iv_at(int_vector, i) + iv_at(&partial_sums, i-1));
    }
    return partial_sums;
}

IntVec generate_interval_sums(IntVec *partial_sums) {
    IntVec interval_sums = iv_new(partial_sums->cap);
        for (size_t i = 0; i < partial_sums->len - 1; i++) {
        for (size_t j = i + 1; j < partial_sums->len; j++) {
        int previous_partial = i == 0
                ? 0
                : iv_at(partial_sums, i - 1);
            iv_append(&interval_sums,
                      iv_at(partial_sums, j) - previous_partial);
        }
    }
    return interval_sums;
}

int int_compare(const void *x, const void *y) {
    int a = *(const int*)x;
    int b = *(const int*)y;
 
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

long long int factorial(long long int x) {
    if (x == 0) {
        return 1;
    }
    for (long long int i = x - 1; i > 1; i--) {
        x *= i;
    }
    return x;
}

long long int binom_2(int x)
                                    { return factorial(x) / (2 * factorial(x - 2)); }

long long int binom_2_fast(int x) {
    if(x <= 0) return 0;
    return (x * (x-1)) / 2;
}

long long int get_pair_count(IntVec *intervals) {
    qsort(intervals->buf, intervals->len, sizeof(int), int_compare);
    // iv_print(intervals);
    int previous = iv_at(intervals, 0);
    int run_len = 1;
    long long int pairs = 0;
    for (size_t i = 1; i < intervals->len; i++) {
        if (iv_at(intervals, i) == previous) {
            run_len += 1;
        } else {
            if (run_len > 1) {
                // printf("Run of %d of length %d (binom %lld)\n", previous,
                //        run_len, binom_2(run_len));
                // pairs += binom_2(run_len);
                pairs += binom_2_fast(run_len);
            }
            previous = iv_at(intervals, i);
            run_len = 1;
        }
    }
    if (run_len > 0) {
        // pairs += binom_2(run_len);
        pairs += binom_2_fast(run_len);
    }
    return pairs;
}

int scan_number (FILE * src, IntVec * int_vector) {
    int scanf_result, num;
    if((scanf_result = fscanf(src," %d", &num)) == 1) {
        iv_append(int_vector, num);

    }
    return scanf_result;
}

int write_result(FILE * dst, IntVec * int_vector) {
    if (int_vector->len == 0) {
        fprintf(dst, "Seznam je prazdny.\n");
        return 0;
    }
    IntVec partial_sums = generate_partial_sums(int_vector);
        IntVec intervals = generate_interval_sums(&partial_sums);
            fprintf(dst, "Pocet dvojic: %lld\n", get_pair_count(&intervals));
    iv_free(&intervals);
    iv_free(&partial_sums);
    return 0;
}

int loadFromFile(FILE * file, IntVec * int_vector) {
    int result;
    while((result = scan_number(file, int_vector)) == 1) { }
    fclose(file);
    return result;
}

FILE * open_file(char * mode) {
    int result;
    char name[51];
    name[50] = '\0';
    result = scanf(" %50s", name);
    if(result != 1 || name[50] != '\0') {
        return NULL;
    }
    return fopen(name, mode);
}

int main(void) {
    int scan_result;
    char command;
    FILE * file;
    IntVec int_vector = iv_new(8);
    printf("Zadejte prikazy:\n");
    while(scanf(" %c", &command) == 1) {
        switch (command) {
            case '<':
                if((file = open_file("r")) == NULL) {
                    iv_free(&int_vector);
                    printf("Nespravny vstup.\n");
                    return -1;
                }
                if(loadFromFile(file, &int_vector) == EOF) {
                    printf("Nacteni probehlo uspesne.\n");
                } else {
                    printf("Nacteni probehlo, ale na konci se vyskytla chyba.\n");
                }
                break;
            case '>':
                if((file = open_file("w")) == NULL) {
                    iv_free(&int_vector);
                    printf("Nespravny vstup.\n");
                    return -1;
                }
                iv_print(file, &int_vector);
                printf("Zapis byl uspesny.\n");
                break;
            case '#':
                write_result(stdout, &int_vector);
                break;
            case '+':
                if(scan_number(stdin, &int_vector) != 1) {
                    iv_free(&int_vector);
                    printf("Nespravny vstup.\n");
                    return -1;
                }
                break;
            case 'P':
                iv_print(stdout, &int_vector);
                break;
            default:
                iv_free(&int_vector);
                printf("Nespravny vstup.\n");
                return -1;
        }
    }

    iv_free(&int_vector);
    return 0;
}
