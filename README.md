# cvičení 1
## Úvod
Tento úkol by vás měl seznámit se základy C++ a jeho rozdíly oproti jazyku C. Budeme zde mít rychlý úvod, bude následovat komplikovanější úloha, za jejíž kompletní splnění bude ohodnoceno bonusovým bodem. V rámci cvičení se nebojte zvednou ruku a na cokoliv (souvisejícího s C++, proboha ;) ) se zeptat.

## GitLab
Na všech cvičeních budeme do jisté míry pracovat s Gitem. Očekáváme jeho znalost díky povinnému předmětu [BI-GIT](https://courses.fit.cvut.cz/BI-GIT/). Pro ty, kteří předmět neměli si doporučujeme alespoň osvojit následující příkazy.
* git clone
* git push
* git commit

Tento repositář si můžete snadno naklonovat pomocí příkazu:
```
git clone https://gitlab.fit.cvut.cz/b222-bi-pa2-lab-11-12/cviceni-1.git
```

Adresu v příkaze je možné nalézt v horní části repositáře pod tlačítkem `Clone`.

## Makefile
Následující laboratorní úlohy budou vždy obsahovat `makefile` soubor pro compilaci přidružených souborů. Ten v tomto cvičení vynecháme, abyste si osvojili příkaz `g++` pro kompilaci.

Pokud by si však někdo chtěl vytvořit makefile pro toto cvičení, bránit tomu rozhodně nebudeme.

## G++ a první kompilace
V tomto adresáři se nachází soubor [my_first_cpp_program.cpp](my_first_cpp_program.cpp). Ten si vyzkoušejte zkompilovat pomocí příkazu `g++` a následně výsledek spusťte. Program by se vás měl zeptat na jméno a následně by vás měl uvítat.

<details>
<summary>Nápověda</summary>

>```shell
>g++ --std=c++17 -Wall -pedantic -O2 -o first.out my_first_cpp_program.cpp
>./first.out
>```
>
>### Vysvětlení
>
>- `g++` - program, compilator používaný ke compilaci C++
>- `--std=c++17` standard ve kterém kompiluje progtest
>- `-Wall` a `-pedantic` - přepínače zajištující, že se vypíšou všechny podezření na chyby
>- `-O2` - přpínač, říkající že se má použít optimizer
>- `-o <file name>` - určení výstupního souboru
</details>

## Něco na rozehřátí
Upravte soubor [my_first_cpp_program.cpp](my_first_cpp_program.cpp) tak, aby se po zadání jména ještě zeptal uživatele na věk. Pokud je věk uživatele větší než 18 let, vypíše hlášku `Let's go celebrate with glass of beer!!!`, v opačném případě vypíše `Let's go celebrate with glass of lemonade!!!`

Nezapomeňte na ověření vstupu, uživatel nemusí nutně zadat číslo, či může svůj věk napsat záporný, v takovém případě by program měl odpovědět `I will return, when you will be honest to me...`, nic jiného by vypsat neměl a rovnou se program ukončí.

### Poznámka
Všimněte si, že v programu od nás nevyužíváme `char *`, ale `string`, kterému nebyla alokovaná paměť, ale záhadně se do něj všechno vejde. Dále je v programu použita funkce `getline`, která chytře načte celý řádek místo toho, aby jen vzala první slovo. C++ obsahuje mnoho obdobných "struktur" a vychytávek, ale o těch si povíme později.

## Úkoly na toto cvičení
V tomto adresáři se nachází soubor [pt4.c](pt4.c). Jedná se o vypracování 4. progtestu z posledního běhu předmětu PA1 s lehkými úpravami. To, jak zadání znělo a jak byl program upraven si můžete přečíst [ZDE](zadani_pt4.md).

Zkopírujte [pt4.c](pt4.c) do souboru `pt4.cpp` a proveďte na něm následující úpravy

### 1) Upravte formát souboru
Jak si jistě všimnete, autor tohoto kódu to dost "zprasil". Upravte formátování kódu tak, aby odpovídalo code rewiew.

<details>
<summary>Nápověda</summary>

> pokud používáte CLion, hodně pomůže klávesová zkratka `Ctrl+Alt+L`
</details>

### 2) Přidejte na začátek souboru namespace pro snadné použití funkcí C++ a použijte C++ verze C knihoven
<details>
<summary>Nápověda</summary>

> ```c++
> using namespace std;
> ```

Z:
```c++
#include<stdio.h>
```

Na:
```c++
#include<cstdio>
```
</details>

### 3) Výpisy a čtení z konzole do stylu C++
V kódu dochází k mnoha výpisům do konzole a čtení z ní pomocí C funkcí (`scanf`, `printf`). Přepište to tak, jak se to dělá v C++
<details>
<summary>Nápověda</summary>

> Příklady můžete vidět v souboru [my_first_cpp_program.cpp](my_first_cpp_program.cpp).
</details>

### 4) Přidání referencí
V kódu je mnohokrát předávána struktura `IntVec` jako pointer. Přepište definice a volání těchto funkcí, aby místo nich tam byli reference.
<details>
<summary>Nápověda</summary>

> Z:
> ```c++
> int fn(IntVec * x) {...}
> ```
> Na:
> ```c++
> int fn(IntVec & x) {...}
> ```
> Je nutné však následně upravit použítí uvnitř funkce (bez `*` nebo `->`).
> 
>Doporučuji si přečíst úkol 7 a vyřešit realizaci referencí naráz.
</details>

### 5) Úprava struktury
V C++ už nově není nutné používat typedef (minimálně v okolí struktur).  
Upravte strukturu tak, aby neměla u sebe keyword `typedef`, však zároveň nebylo nutné upravovat nic jiného v kódu.

<details>
<summary>Nápověda</summary>

> dohledejte v [přednášce](https://courses.fit.cvut.cz/BI-PA2/media/lectures/l01-non_oop-cz.pdf)...

</details>

### 6) Nahraďte dynamické alokace paměti
V kódu jsou použity funkce `malloc`, `realoc` a `free`. Nahraďte je klíčovými slovy `new` a `delete` (popřípadě `delete[]`).

Zde je nutné si opravdu uvědomit rozdíl mezi `delete` a `delete[]`.

### 7) Přepište načítání a zápis do souboru do stylu C++
Aktuálně jsou v kódu použity funkce `fopen`. Místo nich použijte C++ ekvivalenty.

Zároveň zajistěte předávání otevřeného souboru pomocí referencí místo pointeru.

<details>
<summary>Nápověda</summary>

> použijte [`std::fstream`](https://en.cppreference.com/w/cpp/io/basic_fstream)

</details>

## Úkoly mimo scope tohoto cvičení (nebyly probrány na přednáškách)
### 8) Nahraďte všechny `char *` za `std::string`
<details>
<summary>Nápověda</summary>

>Použití bylo načrtnuto v [my_first_cpp_program.cpp](my_first_cpp_program.cpp).

</details>

### 9) Nahraďte funkci `qsort` za její C++ ekvivalent.

<details>
<summary>Nápověda</summary>

> [`std::sort`](https://en.cppreference.com/w/cpp/algorithm/sort)

</details>

## Závěrem
Záměrně jsme se zde vyhnuli úlohám ze [cvičebnice](https://courses.fit.cvut.cz/BI-PA2/tutorials/01.html). Tyto úlohy ponecháváme, aby si student vyzkoušel sám. Pokud jste však došli až sem a zbývá čas, nebojte se si úlohy vyzkoučet na cvičení, zde vám s nimi můžeme poradit, doma už moc ne. To platí i pro následující cvičení.
