#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    string name;
    cout << "Enter your name:" << endl;
    getline(cin, name);
    if(cin.fail()) {
        return 1;
    }
    cout << "Hello " << name << " and welcome to c++" << endl;
    return 0;
}